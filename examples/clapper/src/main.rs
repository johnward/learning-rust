extern crate clap;
use clap::{App, Arg};

fn main() {
    println!("Hello, world!");

    let matches = App::new("My Super Program")
        .version("1.0")
        .author("John Ward <john@johnward.net>")
        .about("Does awesome things")
        .arg(
            Arg::with_name("cells")
                .short("c")
                .long("cells")
                .value_name("CELLS")
                .help("Sets the number of cells")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("PROGRAM")
                .help("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .get_matches();

    println!("Got args");
    let filename = matches.value_of("cells").unwrap();
    println!("Cells: {}", filename);

    let config = matches.value_of("PROGRAM").unwrap_or("default.conf");
    println!("Program Name: {}", config);
}
