//! This is the documentaton for the crate itself
//! It can be mmany line of clde
//! 
//! Woop!
//! 
//! _Woop Again_
//! ==========

#![warn(missing_docs)]


#[derive(Default)]
/// Count things starting from zero
/// 
/// Greate way to count stuff
pub struct Counter {

    value: usize,
}

impl Counter {

    /// Increment the counter
    /// 
    /// ```
    /// # use counter::Counter;
    /// let mut ctr = Counter::default();
    /// ctr.increment();
    /// assert!(ctr.value() == 1);
    /// ```
    pub fn increment(&mut self) {
        self.value += 1;
    }

    /// Get the counter
    pub fn value(&self) -> usize {
        self.value
    }
}

#[cfg(test)]
mod tests {
    use super::Counter;
    #[test]
    fn value_is_correct() {
        let mut ctr = Counter::default();
        assert_eq!(ctr.value(), 0);
        ctr.increment();
        assert_eq!(ctr.value(), 1);
    }

}
