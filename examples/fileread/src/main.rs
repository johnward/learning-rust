use std::env::args;
use std::io;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let filename = args().nth(1).ok_or("file not found")?;

    let contents = std::fs::read_to_string(filename)?;

    let value: Result<isize, _> = contents.lines().map().sum();

    println!("{}", value);

    let contents 
}
