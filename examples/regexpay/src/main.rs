use regex::Regex;
use std::str::FromStr;
use std::env::args;

use lazy_static::lazy_static;

fn main() {
    println!("Hello, world!");

    for arg in args().skip(1) {
        lazy_static! {
            static ref RE: Regex = Regex::from_str("^[A-Z]").unwrap();    
        }
        
        if RE.is_match(&arg) {
            println!("Matched: {}", arg);
        }
    }
}
