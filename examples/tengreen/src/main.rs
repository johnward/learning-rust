use std::env;

fn print_verse(n: usize) {
    let bottle = if n == 1 { "bottle" } else { "bottles" };
    println!("{} green {}", n, bottle);
}

fn main() {

    let bottlecount = env::args().skip(1).next();

    let bottlecount = match bottlecount {
        Some(n) => {
            let n: usize = n.parse().unwrap();

            n
        },
        None => 10
    };

    println!("BottleCount: {}", bottlecount);

    for i in (1..=bottlecount).rev() {
        print_verse(i);
    }
    
}
