use std::env::args;
use std::fs;
use std::io::Result;

fn main() -> Result<()> {
    for arg in args().skip(1) {
        let content = fs::read_to_string(arg)?;
        println!("I read {} characters", content.chars().count());
        print!("{}", content);
    }
    // Aj 🦀🏠
    Ok(())
}
