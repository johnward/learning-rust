use std::env;

/// Say hello to someone.
fn say_hello(name: &str) {
    println!("Hello {}!", name);
}

fn main() {
    say_hello("World");
    for name in env::args().skip(1) {
        say_hello(&name);
    }
}
