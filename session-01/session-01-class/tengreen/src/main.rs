use std::env;

fn print_verse(n: usize) -> () {
    let bottle = if n == 1 { "bottle" } else { "bottles" };
    println!("{} green {}!", n, bottle);
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let bottlecount = env::args().skip(1).next();
    let bottlecount = match bottlecount {
        Some(n) => {
            let n: usize = n.parse()?;
            n + 5
        }
        None => 10,
    };
    for i in (1..=bottlecount).rev() {
        print_verse(i);
    }
    Ok(())
}
