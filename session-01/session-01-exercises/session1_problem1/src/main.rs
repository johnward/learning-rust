use std::env::args;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

fn main() -> std::io::Result<()> {
    let args: Vec<String> = args().collect();
    let filename = &args[1];

    let _path = Path::new(&filename);

    let f = File::open(_path)?;
    let reader = BufReader::new(f);
    let mut total = 0;

    for l in reader.lines() {
        //let number_str : String = l.unwrap();
        //let num : i32 = number_str.parse().unwrap();

        let num: i32 = l.unwrap().parse().unwrap();
        total = total + num;
    }

    println!("Total Number is: {}", total);
    Ok(())
}
