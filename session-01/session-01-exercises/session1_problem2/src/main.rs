use std::env::args;
use std::io;

fn valid_bf_char(c: char) -> Option<char> {
    match c {
        '>' | '<' | '+' | '-' | '.' | ',' | '[' | ']' => Some(c),
        _ => None,
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let args: Vec<String> = args().collect();
    // let filename = &args[1];

    let filename = args().nth(1).ok_or("I need a filename")?;

    let content = std::fs::read_to_string(&filename)?;

    let filtered: String = content.chars().flat_map(valid_bf_char).collect();

    println!("{}", filtered);

    Ok(())
}
