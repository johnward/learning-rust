use std::env::args;
use std::fmt;
use std::option::Option;
use std::result::Result;

#[derive(Debug)]
enum BFCommand {
    IncrementPointer(char),  //>
    DecrementPointer(char),  //<
    IncrementByte(char),     //+
    DecrementByte(char),     //-
    OutputByte(char),        //.
    InputByte(char),         //,
    IfZeroJumpForward(char), //[
    IfNonZeroJumpBack(char), //]
}

impl BFCommand {
    fn from_char(raw_command: char) -> Option<BFCommand> {
        match raw_command {
            '>' => Some(BFCommand::IncrementPointer(raw_command)),
            '<' => Some(BFCommand::DecrementPointer(raw_command)),
            '+' => Some(BFCommand::IncrementByte(raw_command)),
            '-' => Some(BFCommand::DecrementByte(raw_command)),
            '.' => Some(BFCommand::OutputByte(raw_command)),
            ',' => Some(BFCommand::InputByte(raw_command)),
            '[' => Some(BFCommand::IfZeroJumpForward(raw_command)),
            ']' => Some(BFCommand::IfNonZeroJumpBack(raw_command)),
            _ => None,
        }
    }
}

#[derive(Debug)]
struct InputInstruction {
    command: BFCommand,
    line_number: usize,
    column_number: usize,
}

impl InputInstruction {
    pub fn new(command: BFCommand, line_number: usize, column_number: usize) -> InputInstruction {
        InputInstruction {
            command,
            line_number,
            column_number,
        }
    }
}

impl fmt::Display for InputInstruction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "[{}, {}] {}",
            self.line_number,
            self.column_number,
            get_raw_command(&self.command).unwrap()
        )
    }
}

fn get_commands(filename: &String) -> Result<Vec<InputInstruction>, Box<dyn std::error::Error>> {
    let content = std::fs::read_to_string(&filename)?;

    let mut tokens = Vec::new();
    let mut line_num = 0;
    let mut col_num;

    for line in content.lines() {
        col_num = 0;
        //println!("Line {}", line_num);
        for achar in line.chars() {
            //println!("Column {}", col_num);
            match BFCommand::from_char(achar) {
                Some(v) => {
                    let instruction = InputInstruction::new(v, line_num, col_num);
                    tokens.push(instruction)
                }
                None => (),
            }
            col_num += 1;
        }
        line_num += 1;
    }
    Ok(tokens)
}

fn get_raw_command(a_bfcommand: &BFCommand) -> Option<String> {
    match a_bfcommand {
        BFCommand::IncrementPointer('>') => Some(">".to_string()),
        BFCommand::DecrementPointer('<') => Some("<".to_string()),
        BFCommand::IncrementByte('+') => Some("+".to_string()),
        BFCommand::DecrementByte('-') => Some("-".to_string()),
        BFCommand::OutputByte('.') => Some(".".to_string()),
        BFCommand::InputByte(',') => Some(",".to_string()),
        BFCommand::IfZeroJumpForward('[') => Some("[".to_string()),
        BFCommand::IfNonZeroJumpBack(']') => Some("]".to_string()),
        _ => None,
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let filename = args().nth(1).ok_or("I need a filename")?;

    let instructions = get_commands(&filename).unwrap();

    for instruction in instructions.iter() {
        println!("[inputfile {}", instruction);
    }

    Ok(())
}
