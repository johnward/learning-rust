use std::collections::HashMap;
use std::convert::TryFrom;
use std::default::Default;
use std::env::args;

#[derive(Default, Debug)]
struct Score {
    total: u32,
    count: u32,
    missed: u32,
}

impl Score {
    pub fn add_score(&mut self, a_total: u32) {
        self.total += a_total;
    }

    pub fn add_count(&mut self) {
        self.count += 1;
    }

    pub fn missed_test(&mut self) {
        self.missed += 1;
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum Person {
    Data { name: String, number: u32 },
    Name(String),
}

impl TryFrom<String> for Person {
    type Error = &'static str;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.contains(":") {
            let v: Vec<&str> = value.split(":").collect();
            //println!("Data Vector: {:?}", v);
            Ok(Person::Data {
                name: String::from(v[0]),
                number: v[1].parse::<u32>().unwrap(),
            })
        } else {
            Ok(Person::Name(value))
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let filename = args().nth(1).ok_or("I need a filename")?;

    let persons = read_data(&filename).unwrap();

    //println!("Persons: {:?}", persons);

    let mut test_scores: HashMap<String, Score> = HashMap::new();

    for person in persons.iter() {
        match person {
            Person::Data { ref name, number } => {
                let score = test_scores
                    .entry(name.to_string())
                    .or_insert(Score::default());
                //println!("Name {}, Score {}", name.to_string(), number);
                //println!("score.count {}", score.count);
                //println!("score.missed {}", score.missed);
                //println!("score.total {}", score.total);
                score.add_score(*number);
                score.add_count();
            }
            Person::Name(ref name) => {
                let score = test_scores
                    .entry(name.to_string())
                    .or_insert(Score::default());
                //println!("Name {}", name.to_string());
                //println!("score.count {}", score.count);
                //println!("score.missed {}", score.missed);
                //println!("score.total {}", score.total);
                score.add_count();
                score.missed_test();
            }
        };
    }

    //println!("{:?}", test_scores);

    for (name, score) in &test_scores {
        let total_test = if score.count == 1 { "test" } else { "tests" };
        let missed_test = if score.missed == 1 { "test" } else { "tests" };
        println!(
            "{} took {} {}, with a total score of {}. They missed {} {}.",
            name, score.count, total_test, score.total, score.missed, missed_test
        );
    }

    Ok(())
}

fn read_data(filename: &String) -> Result<Vec<Person>, Box<dyn std::error::Error>> {
    let content = std::fs::read_to_string(&filename)?;

    let mut persons = Vec::new();

    for line in content.lines() {
        let a_person = Person::try_from(String::from(line)).unwrap();
        persons.push(a_person);
    }

    Ok(persons)
}
